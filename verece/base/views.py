from django.core.urlresolvers import reverse_lazy
from django.views.generic import DetailView, ListView

from extra_views import FormSetView

from verece.base.forms import TransactionForm
from verece.base.models import Transaction
from verece.base.mixins import CreditorDebtorOnlyMixin, LoginRequiredMixin


class TransactionDetailView(CreditorDebtorOnlyMixin, DetailView):
    """Class based view to show transaction details"""
    model = Transaction
    template_name = 'view_transaction.html'


class TransactionListView(LoginRequiredMixin, ListView):
    """Class based view to list transactions"""
    model = Transaction
    template_name = 'list_transactions.html'

    def get_queryset(self):
        return Transaction.get_for_user(self.request.user)


class TransactionCreateView(LoginRequiredMixin, FormSetView):
    """Class based view for new transactions."""
    form_class = TransactionForm
    template_name = 'create_transaction.html'
    success_url = reverse_lazy('list_transactions')
    extra = 1

    def formset_valid(self, formset):
        for form in formset.forms:
            obj = form.save(commit=False)
            obj.creditor = self.request.user
            obj.save()

        return super(TransactionCreateView, self).formset_valid(formset)
