from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Sum, Q

from djmoney.models.fields import MoneyField


class Transaction(models.Model):
    """Verese transaction model."""
    creditor = models.ForeignKey(User, related_name='creditor')
    debtor = models.ForeignKey(User, related_name='debtor')
    debt = MoneyField(max_digits=10, decimal_places=2, default_currency='EUR')
    created_on = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        ordering = ['-created_on']

    def __unicode__(self):
        return unicode(self.debt)

    def get_absolute_url(self):
        return reverse('view_transaction', kwargs={'pk': self.pk})

    @classmethod
    def get_balance(cls, creditor, debtor):
        """Get balance between two verece users"""
        debt_qs = cls.objects.filter(creditor=creditor, debtor=debtor)
        credit_qs = cls.objects.filter(creditor=debtor, debtor=creditor)

        debt = debt_qs.aggregate(total=Sum('debt'))
        credit = credit_qs.aggregate(total=Sum('debt'))

        return debt['total'] - credit['total']

    @classmethod
    def get_for_user(cls, user):
        """Get all user transactions."""
        query = Q(creditor=user) | Q(debtor=user)
        return cls.objects.filter(query)
