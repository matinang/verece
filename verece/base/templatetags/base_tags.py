from django import template

register = template.Library()


@register.filter
def get_name(obj):
    value = obj.get_full_name() or obj.username or obj.email
    return value
