from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseForbidden
from django.utils.decorators import method_decorator


class LoginRequiredMixin(object):
    """Mixin that allows access only to authorized users."""
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class CreditorDebtorOnlyMixin(LoginRequiredMixin):
    """Mixin that only allows access to transaction's creditor or debtor."""
    def get(self, request, *args, **kwargs):
        response = super(CreditorDebtorOnlyMixin, self).get(request,
                                                            *args, **kwargs)
        if request.user not in [self.object.creditor, self.object.debtor]:
            return HttpResponseForbidden()
        return response
