from django.conf.urls import patterns, url
from django.views.generic import TemplateView

from verece.base.views import (TransactionCreateView,
                               TransactionDetailView, TransactionListView)

urlpatterns = patterns(
    'base',
    url(r'^$', TemplateView.as_view(template_name='base.html'), name='main'),
    url(r'^transactions/$', TransactionListView.as_view(),
        name='list_transactions'),
    url(r'^transactions/(?P<pk>\d+)', TransactionDetailView.as_view(),
        name='view_transaction'),
    url(r'^transactions/new/$', TransactionCreateView.as_view(),
        name='create_transaction')
)
