from django.forms import ModelForm

from verece.base.models import Transaction


class TransactionForm(ModelForm):
    class Meta:
        model = Transaction
        fields = ['debtor', 'debt']
