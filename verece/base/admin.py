from django.contrib import admin

from verece.base.models import Transaction

admin.site.register(Transaction)
